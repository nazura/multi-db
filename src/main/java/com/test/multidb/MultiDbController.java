package com.test.multidb;

import com.test.multidb.domain.main.User;
import com.test.multidb.domain.secondary.City;
import com.test.multidb.repository.main.UserRepository;
import com.test.multidb.repository.secondary.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MultiDbController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CityRepository cityRepository;


    @RequestMapping("/multi/{id}")
    public String fooBar(@PathVariable("id") Long id) {
        User user = userRepository.findById(id);
        City city = cityRepository.findById(id);

        return  user.getName() + " - "  + city.getCountry() + "!";
    }

}
