package com.test.multidb;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "sqldbEntityManagerFactory",
        transactionManagerRef = "sqldbTransactionManager", basePackages = {"com.test.multidb.repository.secondary"})
public class SqlDbConfig {

    @Bean(name = "sqldbDataSource")
    @ConfigurationProperties(prefix = "sqldb.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sqldbEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("sqldbDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource).packages("com.test.multidb.domain.secondary").persistenceUnit("city")
                .build();
    }

    @Bean(name = "sqldbTransactionManager")
    public PlatformTransactionManager barTransactionManager(
            @Qualifier("sqldbEntityManagerFactory") EntityManagerFactory barEntityManagerFactory) {
        return new JpaTransactionManager(barEntityManagerFactory);
    }

}
