package com.test.multidb.repository.secondary;

import com.test.multidb.domain.secondary.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

    City findById(Long id);

}
